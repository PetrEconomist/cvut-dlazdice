#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int min(int, int);
int max(int, int);
int getLengthDelta(int, int, int, int, int, int, int, int);
int calculateLength(int, int, int, int);
int getCeilTileCount(double, int, int, int);
int getGCD(int, int);
int getLCM(int, int);
int getCommonLength(int, int, int, int, int, int, int);
static const int MAX_LENGTH = 100000000;

int main() {
	double inMinX, inMinY;
	inMinX = 10;
	inMinY = 300;
	double inTileA, inTileB, inJointA, inJointB;
	inTileA = 24.6;
	inJointA = 0.2;
	inTileB = 15.8;
	inJointB = 0.2;
	
	int commonLenX = getCommonLength(inTileA * 10, inTileB * 10, inJointA * 10, inJointB * 10, inJointA * 10, inJointB * 10, inMinX * 10);
	printf("result 1: %d\n", commonLenX);
	int LCM = getLCM(inTileA * 10 + inJointA * 10, inTileB * 10 + inJointB * 10);
	printf("lcm A&B %d\n", LCM);
	double inTileC = 16.2;
	double inJointC = 0.4;
	int commonLenX2 = getCommonLength(LCM, inTileC * 10, 0, inJointC * 10, commonLenX, inJointC * 10, inMinX * 10);
	printf("result 2: %d\n", commonLenX2);
}

int getCommonLength(int tileA, int tileB, int jointA, int jointB, int borderA, int borderB, int minLen){
	/* větší se založí, na něj se bude napasovávat menší, tedy maximální odchylka bude o velikosti menší dlaždice */
	// A je větší jak B
	
	if( tileA + jointA < tileB + jointB){
		int tempVal = tileB;
		tileB = tileA;
		tileA = tempVal;
		
		tempVal = jointB;
		jointB = jointA;
		jointA = tempVal;
                
                tempVal = borderB;
		borderB = borderA;
		borderA = tempVal;
                
	}
	
	int countA = getCeilTileCount(minLen, tileA, jointA, borderA);
	int countB = getCeilTileCount(calculateLength(tileA, jointA, borderA, countA), tileB, jointB, borderB);
	printf("calc len A %d\n", calculateLength(tileA, jointA, borderA, countA));
	printf("init counts A:%d len %d B:%d len %d\n", countA, tileA, countB, tileB);
	
	const int SIZE_VARIANCES = tileB + jointB;
	int varianceVisited [SIZE_VARIANCES];
		
	for(int i = 0; i < SIZE_VARIANCES; i++){
		varianceVisited[i]  = 0;
	}
	
	int counter = 0;
	int delta = getLengthDelta(tileA, jointA, borderA, countA, tileB, jointB, borderB, countB);
	while(delta != 0){
		printf("pokus A %dx delka %d a B %dx delka %d, delta %d\n", countA, calculateLength(tileA, jointA, borderA, countA), countB, calculateLength(tileB, jointB, borderB, countB), delta);
		if (varianceVisited[delta] == 0){
			printf("delta %d zápis do %d\n", delta, delta);
			varianceVisited[delta] = 1;
		}else{
			printf("rozdíl %d se vyskytl opakovaně\n", delta);
			/*
			for(int i = 0; i < SIZE_VARIANCES; i++){
				printf("počet výskytu rozdílu %d: %d\n", i, varianceVisited[i]);
			}
			*/
			break;
		}
			
		countA++;
		countB = getCeilTileCount(calculateLength(tileA, jointA, borderA, countA), tileB, jointB, borderB);
		printf("new A count: %d\n", countA);
		
		delta = getLengthDelta(tileA, jointA, borderA, countA, tileB, jointB, borderB, countB);
		counter ++;
		if(calculateLength(tileA, jointA,  borderA, countA) >MAX_LENGTH){
			break;
		}
	}
	
	if(delta == 0){
		printf("reseni: pri poctu dlazdicek A %d a B %d je delka %d\n", countA, countB, calculateLength(tileA, jointA, borderA, countA));
		return calculateLength(tileA, jointA, borderA, countA);
	}else{
		printf("nemá řešení\n");
		return -1;
	}
	
	
   
}

int getCeilTileCount(double totalLength, int tile, int joint, int border){
	return (int) ceil((totalLength - border) / (tile + joint));
}

int getLengthDelta(int tileA, int jointA, int borderA, int countA, int tileB, int jointB, int borderB, int countB){
	return calculateLength(tileB, jointB, borderB, countB) - calculateLength(tileA, jointA, borderA, countA);
}

int calculateLength(int tile, int joint, int border, int tileCount){
	return border + (tile + joint) * tileCount;
}

int min(int a, int b){
	if(a <= b){
		return a;
	}else{
		return b;
	}
}

int max(int a, int b){
	if(a >= b){
		return a;
	}else{
		return b;
	}
}

int getGCD(int n1, int n2){
	for (int i = 1; i <= n1 && i <= n2; ++i) {
        
        // check if i is a factor of both integers
        if (n1 % i == 0 && n2 % i == 0)
            return i;
    }
}

int getLCM(int n1, int n2){
	return (n1 * n2) / getGCD(n1, n2);
}
